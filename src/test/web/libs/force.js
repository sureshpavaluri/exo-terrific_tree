!function() {
    var force = {
        info : "First build, then render"
    };

    force.build = function(w,h,rawData, colors) {
        if(typeof colors === "undefined") colors = {};
        return new ForceGraph(w,h,nodesAndLinks(rawData, colors));
    }

    function ForceGraph(w,h,data) {
        this.graph = d3.layout.force()
            .charge(-360)
            .linkDistance(90)
            .size([w, h])
            .nodes(data.nodes)
            .links(data.links)
            .start();

        this.render = function(svg) {
             var node = svg.selectAll(".node")
                 .data(data.nodes)
                 .enter()
                 .append("g")
                 .call(this.graph.drag);

             node.append("circle")
                 .attr("class", "node")
                 .attr("r", 20)
                 .style("fill", function(d) { return d.color; });

             node.append("text").text(function(d) { return d.name; });

             var link = svg.selectAll(".link")
                 .data(data.links)
                 .enter().append("line")
                 .attr("class", "link")
                 .style("stroke-width", "2");

             this.graph.on("tick", function() {
                 link.attr("x1", function(d) { return d.source.x; })
                     .attr("y1", function(d) { return d.source.y; })
                     .attr("x2", function(d) { return d.target.x; })
                     .attr("y2", function(d) { return d.target.y; });

                 node.attr("transform", function(d) {return "translate(" + d.x + "," + d.y + ")"});
             });
        }

    }

    function nodesAndLinks(rawData, colors) {
        var n = createNodes(rawData, colors);
        return {nodes : n, links : links(n, rawData)};
    }

    function createNodes(rawData, colors) {
        var nodes = [];
        rawData.forEach(function(l) {
            l.forEach(function(n) {
                nodes.push(n);
            });
        });
        return _.uniq(nodes).map(function(n) {return {name:n, color:colorFromName(n, colors)};});
    }

    function links(nodes, rawData) {
        var links = [];
        rawData.forEach(function(l) {
            var sourceIndex = nodeIndex(nodes,l[1]);
            var targetIndex = nodeIndex(nodes,l[0]);
            var rgbInterpolator = d3.interpolateRgb(nodes[sourceIndex].color, nodes[targetIndex].color);
            nodes[targetIndex].color = rgbInterpolator(1/3);
            links.push({source: sourceIndex, target: targetIndex});
        });
        return links;
    }

    function colorFromName(name, colors) {
        if(typeof colors[name] === "undefined") {
            return "#A0A0A0";
        } else {
            return colors[name];
        }
    }

    function nodeIndex(nodes, name) {
        for(var i = 0, l = nodes.length; i < l; i++) {
            if(nodes[i].name === name) {
                return i;
            }
        }
        return -1;
    }

    this.force = force;
}();